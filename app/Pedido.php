<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Produto;
use App\ProdutoPedido;
use Auth;

class Pedido extends Model
{
    public function endereco(){
        return $this->belongsTo("App\Endereco");
    }

    public function getProdutos(){
        $pps = ProdutoPedido::where(['pedido_id' => $this->id])->get();
        $produtos = [];
        foreach($pps as $pp){
            $produto = Produto::find($pp->produto_id);
            $produto->quantidade = $pp->quantidade;
            $preco = ($produto->oferta ? $produto->oferta : $produto->preco);
            $produto->valor = number_format($preco,2,',','.');
            $produto->total = number_format($preco*$produto->quantidade,2,',','.');
            $produtos[] = $produto;
        }
        return $produtos;
    }
    
    public function getTotal(){
        $pps = ProdutoPedido::where(['pedido_id' => $this->id])->get();;
        $total = 0;
        foreach($pps as $pp){
            $produto = Produto::find($pp->produto_id);
            $produto->quantidade = $pp->quantidade;
            $preco = ($produto->oferta ? $produto->oferta : $produto->preco);
            $total += $preco*$produto->quantidade;
        }
        return $total;
    }

    public static function alteraQuantidade($produto_id, $pedido_id, $tipo){
        $pp = ProdutoPedido::where(['pedido_id' => $pedido_id, 'produto_id' => $produto_id])->first();
        if($tipo=='up'){
            $pp->quantidade += 1;
        }else{
            $pp->quantidade -= 1;
        }
        $pp->save();
        $quantidade = $pp->quantidade;

        $produto = Produto::find($produto_id);
        $preco = ($produto->oferta ? $produto->oferta : $produto->preco);

        $retorno['quantidade'] = $quantidade;
        $retorno['preco'] = "R$ ".number_format($preco*$quantidade,2,',','.');

        return $retorno;
    }
    
    public static function getPedido(){
        //Verifica se o pedido já existe na sessão
        $pedido_id = session('pedido_id');
        $usuario_id = Auth::id();
        if($pedido_id!=""){
            $pedido = Pedido::find($pedido_id);
        }
        if(!isset($pedido->id)){
            //Se não existir verifica se existe um pedido desse usuário ainda aberto
            $pedido = Pedido::where(['usuario_id' => $usuario_id, 'status' => 0]);
            if(!isset($pedido->id)){
                $pedido = new Pedido();
                $pedido->usuario_id = $usuario_id;
                $pedido->save();
            }
        }
        session(['pedido_id' => $pedido->id]);
        return $pedido;
    }
}

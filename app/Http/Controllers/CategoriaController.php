<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Categoria;
use DB;

class CategoriaController extends Controller
{
    function lista(){
        return Categoria::All();
    }
    function index(){
        $buscar = \Request::get('buscar');
        $categorias = DB::table("categorias")
                                ->where('nome','like','%'.$buscar.'%')
                                ->get();
        
        return view('admin.categorias', compact('categorias'));
    }

    public function create()
    {
        return view('admin.categoria');
    }

    public function store(Request $request)
    {
        $regras = [
            'nome' => 'required|unique:categorias',
        ];
        $mensagens = [
            'required' => ':attribute é obrigatório!',
            'unique' => ':attribute deve ser único!',
        ];
        $request->validate($regras, $mensagens);
        $categoria = new Categoria();
        $categoria->nome = $request->input('nome');
        $categoria->save();
        return Redirect("/admin/categorias");
    }

    public function edit($id)
    {
        $categoria = Categoria::find($id);
        if(isset($categoria)){
            return view('admin.categoria',compact('categoria'));
        }
        return redirect('/admin/categorias');
    }

    public function update(Request $request, $id)
    {
        $regras = [
            'nome' => 'required',
        ];
        $mensagens = [
            'required' => ':attribute é obrigatório!',
            'unique' => ':attribute deve ser único!',
        ];
        $request->validate($regras, $mensagens);
        $categoria = Categoria::find($id);
        if(isset($categoria)){
            $categoria->nome = $request->input('nome');
            $categoria->save();
        }
        return redirect('/admin/categorias');
    }

    public function destroy($id)
    {
        $categoria = Categoria::find($id);
        if(isset($categoria)){
            $categoria->delete();
            return response("OK", 200);
        }
        return response("Categoria não encontrada", 404);
    }
}

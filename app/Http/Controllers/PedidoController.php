<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Pedido;
use App\ProdutoPedido;
use App\Endereco;


class PedidoController extends Controller
{
    function index($todos=false){
        $user = Auth::user();
        $usuario_id = Auth::id();
        if($user->type=="admin"&&$todos=="admin"){
            $pedidos = Pedido::where("status", 1)->orderBy('updated_at', 'desc')->get();
        }else{
            $pedidos = Pedido::where(["status" => 1, 'usuario_id' => $usuario_id])->orderBy('updated_at', 'desc')->get();
        }
        return view('pedidos', compact('pedidos'));
    }

    function view($id){
        $pedido = Pedido::find($id);
        $produtos = $pedido->getProdutos();
        $total = number_format($pedido->getTotal(),2,',','.');
        $endereco = Endereco::find($pedido->endereco_id);
        $pedido = $pedido->id;
        $edit = false;

        return view('carrinho', compact(['produtos','total','pedido','edit','endereco']));
    }

    public function carrinho(){
        $pedido = Pedido::getPedido();
        $produtos = $pedido->getProdutos();
        $total = number_format($pedido->getTotal(),2,',','.');
        $pedido = $pedido->id;
        $edit = true;

        return view('carrinho', compact(['produtos','total','pedido','edit']));
    }

    public function store(Request $request, $id){
        session(['pedido_id' => '']);
        $pedido = Pedido::find($id);
        if(isset($pedido)){
            $regras = [
                'rua' => 'required',
                'numero' => 'required',
                'cep' => 'required',
                'bairro' => 'required',
                'estado' => 'required',
                'cidade' => 'required',
            ];
            $mensagens = [
                'required' => ':attribute é obrigatório!',
            ];
            $request->validate($regras, $mensagens);
            $endereco = new Endereco();
            $endereco->rua = $request->input('rua');
            $endereco->numero = $request->input('numero');
            $endereco->cep = $request->input('cep');
            $endereco->bairro = $request->input('bairro');
            $endereco->estado = $request->input('estado');
            $endereco->cidade = $request->input('cidade');
            $endereco->save();
            $pedido->status = 1;
            $pedido->endereco_id = $endereco->id;
            $pedido->total = $pedido->getTotal();
            $pedido->save();
            return redirect('/obrigado');
        }
        return response("Produto não encontrado", 404);
    }

    public function alteraQuantidade(Request $request, $tipo)
    {
        $produto_id = $request->input('produto');
        $pedido_id = $request->input('pedido');

        $pedido = Pedido::find($pedido_id);
        if(isset($pedido)){
            $retorno = $pedido->alteraQuantidade($produto_id, $pedido_id, $tipo);

            $total = $pedido->getTotal();
            $retorno['total'] = "R$ ".number_format($total,2,',','.');
            return json_encode($retorno);
        }
        return response("Pedido não encontrado", 404);
    }

    public function deletaProduto(Request $request)
    {
        $produto_id = $request->input('produto');
        $pedido_id = $request->input('pedido');

        $pp = ProdutoPedido::where(['pedido_id' => $pedido_id, 'produto_id' => $produto_id])->first();
        if(isset($pp)){
            $pp->delete();
            $pedido = Pedido::find($pedido_id);
            $total = $pedido->getTotal();
            $retorno['total'] = "R$ ".number_format($total,2,',','.');
            return json_encode($retorno);
        }
        return response("Produto não encontrado", 404);
    }
}

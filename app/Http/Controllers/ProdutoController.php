<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Image;
use Storage;
use DB;
use Auth;
use App\Categoria;
use App\Caracteristica;
use App\Produto;
use App\Pedido;
use App\ProdutoPedido;

class ProdutoController extends Controller
{
    function home(){
        $produtos = DB::table("produtos")
                            ->orderBy('id', 'desc')
                            ->limit(6)
                            ->get();
        return view('home', compact('produtos'));
    }

    function search($filtro = false){
        $buscar = \Request::get('q');
        if(!$filtro){
            $produtos = DB::table("produtos")
                                ->where('nome','like','%'.$buscar.'%')
                                ->get();
        }else{
            if(is_numeric($filtro)){
                $produtos = DB::table("produtos")
                    ->join('categoria_produtos', 'produtos.id', '=', 'categoria_produtos.produto_id')
                    ->where('categoria_produtos.categoria_id','=',$filtro)
                    ->get();
            }else{
                $produtos = DB::table("produtos")
                ->whereNotNull('oferta')
                ->get();
            }
        }
        
        return view('home', compact('produtos'));
    }

    function searchCategoria($id){
        return $this->search($id);
    }

    function searchPromocao(){
        return $this->search("promocao");
    }

    function view($id){
        $produto = Produto::find($id);
        if(isset($produto)){
            return view('produto', compact('categorias','caracteristicas','produto'));
        }
        return redirect('/');
    }

    function index(){
        $buscar = \Request::get('buscar');
        $produtos = DB::table("produtos")
                                ->where('nome','like','%'.$buscar.'%')
                                ->get();
        
        return view('admin.produtos', compact('produtos'));
    }

    public function create()
    {
        $categorias = Categoria::all();
        $caracteristicas = Caracteristica::all();
        return view('admin.produto', compact('categorias','caracteristicas'));
    }

    public function store(Request $request)
    {
        $regras = [
            'nome' => 'required',
            'preco' => 'required',
        ];
        $mensagens = [
            'required' => ':attribute é obrigatório!',
        ];
        $request->validate($regras, $mensagens);
        $produto = new Produto();
        $produto->nome = $request->input('nome');
        $produto->preco = $request->input('preco');
        $produto->oferta = $request->input('oferta');
        $produto->descricao = $request->input('descricao');
        $produto->save();
        $categorias = array_values(array_filter($request->input('categoria'), function($var){
            return $var;
        }));
        $produto->categorias()->sync($categorias);
        $caracteristicas = array_values(array_filter($request->input('caracteristica'), function($var){
            return $var;
        }));
        $caracteristicas_com_pivot = [];
        foreach($caracteristicas as $caracteristica){
            $caracteristicas_com_pivot[$caracteristica] = array("valor" => $request->input('unidade')[$caracteristica]);
        }
        $produto->caracteristicas()->sync($caracteristicas_com_pivot);

        if ($request->hasFile('imagem')) {
            $image      = $request->file('imagem');
            $fileName   = $produto->id . '.jpg';

            $img = Image::make($image->getRealPath());
            $img->resize(400, 400, function ($constraint) {
                $constraint->aspectRatio();                 
            });

            $img->stream();

            //dd();
            Storage::disk('s3')->put($fileName, (string) $img);
        }
        return Redirect("/admin/produtos");
    }

    public function edit($id)
    {
        $produto = Produto::find($id);
        if(isset($produto)){
            $categorias = Categoria::all();
            $caracteristicas = Caracteristica::all();
            return view('admin.produto', compact('categorias','caracteristicas','produto'));
        }
        return redirect('/admin/produtos');
    }

    public function update(Request $request, $id)
    {
        $regras = [
            'nome' => 'required',
            'preco' => 'required',
        ];
        $mensagens = [
            'required' => ':attribute é obrigatório!',
        ];
        $request->validate($regras, $mensagens);
        $produto = Produto::find($id);

        $produto->nome = $request->input('nome');
        $produto->preco = $request->input('preco');
        $produto->oferta = $request->input('oferta');
        $produto->descricao = $request->input('descricao');
        $categorias = array_values(array_filter($request->input('categoria'), function($var){
            return $var;
        }));
        $produto->categorias()->sync($categorias);
        $caracteristicas = array_values(array_filter($request->input('caracteristica'), function($var){
            return $var;
        }));
        $caracteristicas_com_pivot = [];
        foreach($caracteristicas as $caracteristica){
            $caracteristicas_com_pivot[$caracteristica] = array("valor" => $request->input('unidade')[$caracteristica]);
        }
        $produto->caracteristicas()->sync($caracteristicas_com_pivot);

        if ($request->hasFile('imagem')) {
            $image      = $request->file('imagem');
            $fileName   = $produto->id . '.jpg';

            $img = Image::make($image->getRealPath());
            $img->resize(400, 400, function ($constraint) {
                $constraint->aspectRatio();                 
            });

            $img->stream();

            //dd();
            Storage::disk('s3')->put($fileName, (string) $img);
        }

        $produto->save();
        return Redirect("/admin/produtos");
    }

    public function destroy($id)
    {
        $produto = Produto::find($id);
        if(isset($produto)){
            $produto->delete();
            return response("OK", 200);
        }
        return response("Produto não encontrado", 404);
    }

    public function image($id)
    {
        $produto = Produto::find($id);
        if(isset($produto)){
            return $produto->getImage();
        }
        return response("Imagem não encontrada", 404);
    }

    public function addCarrinho($id)
    {
        /*session(['pedido_id' => '']);
        dd($pedido_id);*/
        $produto = Produto::find($id);
        $usuario_id = Auth::id();
        
        if(isset($produto)&&$usuario_id){
            $pedido = Pedido::getPedido();
            $pp = ProdutoPedido::where(['pedido_id' => $pedido->id, 'produto_id' => $produto->id])->first();
            if(!isset($pp->pedido_id)){
                $pp = new ProdutoPedido();
                $pp->pedido_id = $pedido->id;
                $pp->produto_id = $id;
                $pp->quantidade = 0;
            }
            $pp->quantidade = $pp->quantidade + 1;
            $pp->save();
            return redirect('carrinho');
        }
        return redirect('/login');
    }
}

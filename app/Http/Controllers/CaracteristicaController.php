<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Caracteristica;
use DB;

class CaracteristicaController extends Controller
{
    function index(){
        $buscar = \Request::get('buscar');
        $caracteristicas = DB::table("caracteristicas")
                                ->where('nome','like','%'.$buscar.'%')
                                ->get();
        
        return view('admin.caracteristicas', compact('caracteristicas'));
    }

    public function create()
    {
        return view('admin.caracteristica');
    }

    public function store(Request $request)
    {
        $regras = [
            'nome' => 'required|unique:caracteristicas',
            'unidade' => 'required',
        ];
        $mensagens = [
            'required' => ':attribute é obrigatório!',
            'unique' => ':attribute deve ser único!',
        ];
        $request->validate($regras, $mensagens);
        $caracteristica = new Caracteristica();
        $caracteristica->nome = $request->input('nome');
        $caracteristica->unidade = $request->input('unidade');
        $caracteristica->save();
        return Redirect("/admin/caracteristicas");
    }

    public function edit($id)
    {
        $caracteristica = Caracteristica::find($id);
        if(isset($caracteristica)){
            return view('admin.caracteristica',compact('caracteristica'));
        }
        return redirect('/admin/caracteristicas');
    }

    public function update(Request $request, $id)
    {
        $regras = [
            'nome' => 'required',
            'unidade' => 'required',
        ];
        $mensagens = [
            'required' => ':attribute é obrigatório!',
            'unique' => ':attribute deve ser único!',
        ];
        $request->validate($regras, $mensagens);
        $this->validar($request);
        $caracteristica = Caracteristica::find($id);
        if(isset($caracteristica)){
            $caracteristica->nome = $request->input('nome');
            $caracteristica->save();
        }
        return redirect('/admin/caracteristicas');
    }

    public function destroy($id)
    {
        $caracteristica = Caracteristica::find($id);
        if(isset($caracteristica)){
            $caracteristica->delete();
            return response("OK", 200);
        }
        return response("Caracteristica não encontrada", 404);
    }
}

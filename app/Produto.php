<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;
use Storage;

class Produto extends Model
{
    public function valorFormatado($tipo='preco'){
        if(is_numeric($this->$tipo)){
            return number_format($this->$tipo,2,',','.');
        }
        return "";
    }

    public function temCategoria($id){
        foreach($this->categorias as $categoria){
            if($categoria->id==$id){
                return true;
            }
        }
        return false;
    }

    public function categorias(){
        return $this->belongsToMany("App\Categoria","categoria_produtos");
    }

    public function temCaracteristica($id){
        foreach($this->caracteristicas as $caracteristica){
            if($caracteristica->id==$id){
                return true;
            }
        }
        return false;
    }

    public function caracteristicas(){
        return $this->belongsToMany("App\Caracteristica","caracteristica_produtos")->withPivot(['valor']);
    }

    public function getImage()
    {
        $imagePath = $this->id.'.jpg';
        $link = Storage::disk('s3')->get($imagePath);
        return Image::make($link)->response();
    }
}

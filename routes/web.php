<?php

//Auth
Auth::routes();

//Index
Route::get('/', 'ProdutoController@home');

//Rotas protegidas para Admin
Route::middleware('is_admin')->group(function(){
    Route::get('admin', function(){
        return view('admin');
    });
    
    //Categorias
    Route::get('admin/categorias', 'CategoriaController@index')->name('categorias');
    Route::get('admin/categorias/criar', 'CategoriaController@create')->name('criar-categoria');
    Route::get('admin/categorias/editar/{id}','CategoriaController@edit');
    Route::post('admin/categorias', 'CategoriaController@store');
    Route::post('admin/categorias/{id}', 'CategoriaController@update');

    //Caracteristicas
    Route::get('admin/caracteristicas', 'CaracteristicaController@index')->name('caracteristicas');
    Route::get('admin/caracteristicas/criar', 'CaracteristicaController@create')->name('criar-caracteristica');
    Route::get('admin/caracteristicas/editar/{id}','CaracteristicaController@edit');
    Route::post('admin/caracteristicas', 'CaracteristicaController@store');
    Route::post('admin/caracteristicas/{id}', 'CaracteristicaController@update');

    //Produtos
    Route::get('admin/produtos', 'ProdutoController@index')->name('produtos');
    Route::get('admin/produtos/criar', 'ProdutoController@create')->name('criar-produto');
    Route::get('admin/produtos/editar/{id}','ProdutoController@edit');
    Route::post('admin/produtos', 'ProdutoController@store');
    Route::post('admin/produtos/{id}', 'ProdutoController@update');

    //Pedidos
    Route::get('/pedidos/{todos}', 'PedidoController@index');
});

/*
    LOJA
*/
Route::get('/produtos/imagem/{id}','ProdutoController@image');
Route::get('/busca', 'ProdutoController@search');
Route::get('/categoria/{id}', 'ProdutoController@searchCategoria');
Route::get('/promocoes', 'ProdutoController@searchPromocao');
Route::get('/produtos/{id}', 'ProdutoController@view');
Route::get('/produtos/adicionar/{id}', 'ProdutoController@addCarrinho');
Route::get('/carrinho', 'PedidoController@carrinho');
Route::post('/carrinho/{id}', 'PedidoController@store');
Route::get('/obrigado', function(){
    return view('obrigado');
});
Route::get('/pedidos', 'PedidoController@index');
Route::get('/pedido/{id}', 'PedidoController@view');
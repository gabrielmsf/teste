<?php

use Illuminate\Database\Seeder;

class ProdutosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produtos')->insert([
            'nome' => 'Mesa Bonita',
            'preco' => 200,
        ]);
        DB::table('categoria_produtos')->insert([
            'categoria_id' => 1,
            'produto_id' => 1,
        ]);
        DB::table('caracteristica_produtos')->insert([
            'caracteristica_id' => 1,
            'produto_id' => 1,
            'valor' => '100',
        ]);
        DB::table('caracteristica_produtos')->insert([
            'caracteristica_id' => 2,
            'produto_id' => 1,
            'valor' => '75',
        ]);
        DB::table('caracteristica_produtos')->insert([
            'caracteristica_id' => 3,
            'produto_id' => 1,
            'valor' => '120',
        ]);
        DB::table('produtos')->insert([
            'nome' => 'Cadeira Bonita',
            'preco' => 120,
            'oferta' => 90,
        ]);
        DB::table('categoria_produtos')->insert([
            'categoria_id' => 2,
            'produto_id' => 2,
        ]);
        DB::table('caracteristica_produtos')->insert([
            'caracteristica_id' => 1,
            'produto_id' => 2,
            'valor' => '100',
        ]);
        DB::table('caracteristica_produtos')->insert([
            'caracteristica_id' => 2,
            'produto_id' => 2,
            'valor' => '75',
        ]);
        DB::table('caracteristica_produtos')->insert([
            'caracteristica_id' => 3,
            'produto_id' => 2,
            'valor' => '120',
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class CaracteristicasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('caracteristicas')->insert([
            'nome' => 'Altura',
            'unidade' => 'cm',
        ]);
        DB::table('caracteristicas')->insert([
            'nome' => 'Largura',
            'unidade' => 'cm',
        ]);
        DB::table('caracteristicas')->insert([
            'nome' => 'Profundidade',
            'unidade' => 'cm',
        ]);
    }
}

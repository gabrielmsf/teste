<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Gabriel Freitas',
            'email' => 'gabrielmsf@gmail.com',
            'password' => bcrypt('secret'),
            'type' => 'admin'
        ]);
        DB::table('users')->insert([
            'name' => 'Comprador 1',
            'email' => 'comprador@gmail.com',
            'password' => bcrypt('secret')
        ]);
    }
}

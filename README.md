# README

O Sistema foi desenvolvido utilizando Laravel em PHP 7.2, Mysql 5.7 e Apache. Foi desenvolvido utilizando o php artisan serve.


Os comandos abaixo instalam os pacotes necessários


sudo apt-get install php7.2 php7.2-common php7.2-cli php7.2-fpm apache2 


sudo apt-get install libapache2-mod-php7.2 php7.2-xml php7.2-opcache php7.2-mbstring


sudo apt-get install php7.2-mysql mysql-server-5.7 mysql-client-5.7 php7.2-gd


Criar um usuário chamado laravel sem senha no mysql e uma tabela chamada mobly


Renomear o arquivo .env.example para .env


Rodar 


    composer install
    
    
    npm run rev
    
    
    php artisan migrate:fresh --seed
    
    
    php artisan serve


Com isso o código deve estar acessível em http://localhost:8000
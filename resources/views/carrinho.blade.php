@extends('layouts.app', ['current' => 'carrinho'])

@section('content')
<div class="container">
    <div class="row">
        <h2 class="col-12 text-center">
            Carrinho
        </h2>
        <div class="col-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Quantidade</th>
                        <th>Preço</th>
                        <th>Total</th>
                        @if($edit)<th></th>@endif
                    </tr>
                </thead>
                <tbody>
                @foreach($produtos as $produto)
                    <tr id="linha_{{$produto->id}}">
                        <td>{{$produto->nome}}</td>
                        <td id="quantidade_{{$produto->id}}">{{$produto->quantidade}}</td>
                        <td>R$ {{$produto->valor}}</td>
                        <td id="total_{{$produto->id}}">R$ {{$produto->total}}</td>
                        @if($edit)<td>
                            <a href="#" class="btn btn-success alterar" data-tipo="up" data-produto="{{$produto->id}}">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </a>
                            <a href="#" class="btn btn-danger alterar" data-tipo="down" data-produto="{{$produto->id}}">
                                <i class="fa fa-minus" aria-hidden="true"></i>
                            </a>
                        </td>@endif
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td>Total</td>
                        <td></td>
                        <td></td>
                        <td id="total_pedido">R$ {{$total}}</td>
                        @if($edit)<td></td>@endif
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="col-12 mt-5">
            <h4>Dados de Endereço</h4>
            <form action="/carrinho/{{$pedido}}" class="row" method="POST">
                {{ csrf_field() }}
                <div class="form-group col-md-4">
                    <label for="rua">Rua</label>
                    <input type="text" class="form-control  @if($errors->has('rua')) is-invalid @endif" name="rua" id="rua" @if(!$edit) value="{{$endereco->rua}}" disabled="disabled" @endif>
                    @if($errors->has('rua'))
                    <div class="invalid-feedback">
                        {{$errors->first('rua')}}
                    </div>
                    @endif
                </div>
                <div class="form-group col-md-4">
                    <label for="numero">Número</label>
                    <input type="text" class="form-control  @if($errors->has('numero')) is-invalid @endif" name="numero" id="numero" @if(!$edit) value="{{$endereco->numero}}" disabled="disabled" @endif>
                    @if($errors->has('numero'))
                    <div class="invalid-feedback">
                        {{$errors->first('numero')}}
                    </div>
                    @endif
                </div>
                <div class="form-group col-md-4">
                    <label for="cep">CEP</label>
                    <input type="text" class="form-control  @if($errors->has('cep')) is-invalid @endif" name="cep" id="cep" @if(!$edit) value="{{$endereco->cep}}" disabled="disabled" @endif>
                    @if($errors->has('cep'))
                    <div class="invalid-feedback">
                        {{$errors->first('cep')}}
                    </div>
                    @endif
                </div>
                <div class="form-group col-md-4">
                    <label for="bairro">Bairro</label>
                    <input type="text" class="form-control  @if($errors->has('bairro')) is-invalid @endif" name="bairro" id="bairro" @if(!$edit) value="{{$endereco->bairro}}" disabled="disabled" @endif>
                    @if($errors->has('bairro'))
                    <div class="invalid-feedback">
                        {{$errors->first('bairro')}}
                    </div>
                    @endif
                </div>
                <div class="form-group col-md-4">
                    <label for="estado">Estado</label>
                    <input type="text" class="form-control  @if($errors->has('estado')) is-invalid @endif" name="estado" id="estado" @if(!$edit) value="{{$endereco->estado}}" disabled="disabled" @endif>
                    @if($errors->has('estado'))
                    <div class="invalid-feedback">
                        {{$errors->first('estado')}}
                    </div>
                    @endif
                </div>
                <div class="form-group col-md-4">
                    <label for="cidade">Cidade</label>
                    <input type="text" class="form-control  @if($errors->has('cidade')) is-invalid @endif" name="cidade" id="cidade" @if(!$edit) value="{{$endereco->cidade}}" disabled="disabled" @endif>
                    @if($errors->has('cidade'))
                    <div class="invalid-feedback">
                        {{$errors->first('cidade')}}
                    </div>
                    @endif
                </div>
                @if($edit)
                <div class="col-12">
                    <button type="submit" class="btn btn-success">Fechar Pedido</button>
                </div>
                @endif
            </form>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(".alterar").click(function(e){
        e.preventDefault();
        var id = $(this).data('produto');
        var tipo = $(this).data('tipo');
        var quantidade = $("#quantidade_"+id).html();
        if(quantidade==1&&tipo=='down'){
            if(window.confirm('Você deseja remover esse produto do seu carrinho?')){
                $.ajax({
                type: "DELETE",
                url: "/api/carrinho/",
                data: {"produto":id,"pedido":{{$pedido}}},
                context: this,
                success: function(retorno){
                    dados = JSON.parse(retorno);
                    $("#total_pedido").html(dados.total);
                    $("#linha_"+id).remove();
                },
                error: function(error){
                    console.log(error);
                },
            }); 
            }
        }else{
            $.ajax({
                type: "PUT",
                url: "/api/carrinho/"+tipo,
                data: {"produto":id,"pedido":{{$pedido}}},
                context: this,
                success: function(retorno){
                    dados = JSON.parse(retorno);
                    $("#quantidade_"+id).html(dados.quantidade);
                    $("#total_"+id).html(dados.preco);
                    $("#total_pedido").html(dados.total);
                },
                error: function(error){
                    console.log(error);
                },
            });
        }
    });
</script>
@endsection

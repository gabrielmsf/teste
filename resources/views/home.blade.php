@extends('layouts.app', ['current' => 'home'])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach($produtos as $produto)
            @component('layouts.produto', ['id' => $produto->id, 'nome' => $produto->nome, 'descricao' => $produto->descricao, 'preco' => number_format($produto->preco, 2, ',', '.'), 'oferta' => number_format($produto->oferta, 2, ',', '.')])
            @endcomponent
        @endforeach
    </div>
</div>
@endsection

@extends('layouts.app', ['current' => 'carrinho'])

@section('content')

<div class="container">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">Obrigado!</div>

                <div class="card-body">
                    O seu pedido foi feito com sucesso, aguarde o nosso contato =)
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
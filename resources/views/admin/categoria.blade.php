@extends('layouts.app', ['current' => 'admin'])

@section('content')
<div class="container">
    @component('layouts.admin.menu', ['current' => 'categoria'])
    @endcomponent
    <div class="row">
        <div class="col">
            <div class="card border">
                <div class="card-body">
                    <form action="/admin/categorias/@if(isset($categoria)){{$categoria->id}}@endif" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="nome">Nome da Categoria</label>
                            <input type="text" class="form-control @if($errors->has('nome')) is-invalid @endif" name="nome" value="@if(isset($categoria)) {{$categoria->nome}}@endif">
                            @if($errors->has('nome'))
                            <div class="invalid-feedback">
                                {{$errors->first('nome')}}
                            </div>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.app', ['current' => 'admin'])

@section('content')
<div class="container">
    @component('layouts.admin.menu', ['current' => 'caracteristica'])
    @endcomponent
    <div class="row">
        <div class="col">
            <div class="card border">
                <div class="card-body">
                    <form action="/admin/caracteristicas/@if(isset($caracteristica)){{$caracteristica->id}}@endif" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="nome">Nome da Caracteristica</label>
                            <input type="text" class="form-control @if($errors->has('nome')) is-invalid @endif" name="nome" value="@if(isset($caracteristica)) {{$caracteristica->nome}}@endif">
                            @if($errors->has('nome'))
                            <div class="invalid-feedback">
                                {{$errors->first('nome')}}
                            </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="unidade">Unidade</label>
                            <input type="text" class="form-control @if($errors->has('unidade')) is-invalid @endif" name="unidade" value="@if(isset($caracteristica)) {{$caracteristica->unidade}}@endif">
                            @if($errors->has('unidade'))
                            <div class="invalid-feedback">
                                {{$errors->first('unidade')}}
                            </div>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.app', ['current' => 'admin'])

@section('content')
<div class="container">
    @component('layouts.admin.menu', ['current' => 'produto'])
    @endcomponent
    <div class="row">
        <div class="col">
            <div class="card border">
                <div class="card-body">
                    <form action="/admin/produtos/@if(isset($produto)){{$produto->id}}@endif" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="nome">Nome do Produto</label>
                            <input type="text" class="form-control @if($errors->has('nome')) is-invalid @endif" name="nome" placeholder="Produto" value="@if(isset($produto)) {{$produto->nome}} @endif">
                            @if($errors->has('nome'))
                            <div class="invalid-feedback">
                                {{$errors->first('nome')}}
                            </div>
                            @endif
                        </div>
                        @if(!isset($produto))
                        <div class="form-group">
                            <label for="imagem">Imagem do Produto</label>
                            <input type="file" class="form-control" name="imagem" accept="image/*" >
                        </div>
                        @else
                        <div class="form-group text-center" id="imagem-atual">
                            <a href="#" id="trocar-imagem" class="btn btn-primary mb-2">Trocar Imagem</a>
                            <img src="/produtos/imagem/{{$produto->id}}" class="d-block mx-auto"/>
                        </div>
                        <div class="form-group" style="display:none" id="nova-imagem">
                            <a href="#" id="voltar-imagem" class="btn btn-primary">Voltar</a>
                            <label for="imagem">Imagem do Produto</label>
                            <input type="file" class="form-control" name="imagem" accept="image/*" >
                        </div>
                        @endif
                        <div class="form-group">
                            <label for="preco">Preço</label>
                            <input type="number" class="form-control @if($errors->has('preco')) is-invalid @endif" name="preco" placeholder="0.0" step="0.01" value="@if(isset($produto)){{$produto->preco}}@endif">
                            @if($errors->has('preco'))
                            <div class="invalid-feedback">
                                {{$errors->first('preco')}}
                            </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="oferta">Preço Promocional</label>
                            <input type="number" class="form-control" name="oferta" placeholder="0.0" step="0.01" value="@if(isset($produto)){{$produto->oferta}}@endif">
                        </div>
                        <div class="form-group">
                            <label for="descricao">Descrição</label>
                            <textarea class="form-control" name="descricao">@if(isset($produto)){{$produto->descricao}}@endif</textarea>
                        </div>
                        <div class="row">
                            <div class="form-group col-md">
                                <label for="categorias">Categorias</label>
                                <div class="list-group lista-categorias">
                                    @foreach($categorias as $categoria)
                                    <a class="list-group-item list-group-item-action @if(isset($produto))@if($produto->temCategoria($categoria->id)) active @endif @endif" data-id="{{$categoria->id}}" id="linha-categoria-{{$categoria->id}}">
                                        {{$categoria->nome}}
                                        <input type="hidden" value="@if(isset($produto))@if($produto->temCategoria($categoria->id)){{$categoria->id}}@else{{0}}@endif @else{{0}}@endif" name="categoria[]" id="categoria-{{$categoria->id}}" />
                                    </a>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group col-md">
                                <label for="caracteristicas">Caracteristicas</label>
                                <div class="list-group lista-caracteristicas">
                                    @foreach($caracteristicas as $caracteristica)
                                    <a class="list-group-item list-group-item-action @if(isset($produto))@if($produto->temCaracteristica($caracteristica->id)) active @endif @endif" data-id="{{$caracteristica->id}}" id="linha-caracteristica-{{$caracteristica->id}}">
                                        <div class="row">
                                            <div class="col-3">
                                                {{$caracteristica->nome}}
                                            </div>
                                            <div class="col-9 text-right">
                                                <input type="text" value="@if(isset($produto))@if(!is_null($produto->caracteristicas()->find($caracteristica->id))){{$produto->caracteristicas()->find($caracteristica->id)->pivot->valor}}@endif @endif" name="unidade[{{$caracteristica->id}}]" /> 
                                                {{$caracteristica->unidade}}
                                                <input type="hidden" value="@if(isset($produto))@if($produto->temCaracteristica($caracteristica->id)){{$caracteristica->id}}@else{{0}}@endif @else{{0}}@endif" name="caracteristica[]" id="caracteristica-{{$caracteristica->id}}" />
                                            </div>
                                        </div>
                                    </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$(".lista-categorias a, .lista-caracteristicas a").click(function(e){
    e.preventDefault();

    var id = $(this).data('id');
    var atual = $(this).find('input:last').val();
    if(atual==0){
        $(this).addClass("active");
        $(this).find('input:last').val(id);
    }else{
        $(this).removeClass("active");
        $(this).find('input:last').val(0);
    }
});

$("#trocar-imagem").click(function(e){
    e.preventDefault();

    $("#imagem-atual").hide();
    $("#nova-imagem").show();
});

$("#voltar-imagem").click(function(e){
    e.preventDefault();

    $("#imagem-atual").show();
    $("#nova-imagem").hide();
});
</script>
<style>
.list-group-item.active{
    color: #FFF !important;
}
</style>
@endsection
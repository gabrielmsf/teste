@extends('layouts.app', ['current' => 'admin'])

@section('content')
<div class="container">
    @component('layouts.admin.menu', ['current' => 'categoria'])
    @endcomponent
    <div class="row">
        <div class="col">
            <div class="card border">
                <div class="card-body">
                    <h5 class="card-title">Cadastro de Categorias</h5>
                    @component('layouts.admin.busca')
                    @endcomponent
                    @if(count($categorias)>0)
                    <table class="table table-ordered table-hover">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Nome</th>
                                <th width="200">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categorias as $categoria)
                                <tr id="linha_{{ $categoria->id }}">
                                    <td>{{ $categoria->id }}</td>
                                    <td>{{ $categoria->nome }}</td>
                                    <td>
                                        <a href="/admin/categorias/editar/{{ $categoria->id }}" class="btn btn-sm btn-primary">Editar</a>
                                        <a href="#" class="btn btn-sm btn-danger apagar_produto" data-id="{{ $categoria->id }}" data-nome="{{ $categoria->nome }}">Apagar</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
                <div class="card-footer">
                    <a href="{{ route('criar-categoria') }}" class="btn btn-sm btn-primary" role="button">Nova Categoria</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        $(".apagar_produto").click(function(e){
            e.preventDefault();
            var id = $(this).data('id');
            var nome = $(this).data('nome');
            if(window.confirm("Você tem certeza que deseja apagar "+nome+"?")){
                $.ajax({
                    type: "DELETE",
                    url: "/api/admin/categorias/"+id,
                    context: this,
                    success: function(){
                        $("#linha_"+id).remove();
                    },
                    error: function(error){
                        console.log(error);
                    },
                });
            }
        });
    });
</script>
@endsection
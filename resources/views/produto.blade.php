@extends('layouts.app', ['current' => ''])

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>{{$produto->nome}}</h1>
            <div class="row">
                <div class="col-6">
                    <img src="/produtos/imagem/{{$produto->id}}" alt="{{$produto->nome}}"/>
                </div>
                <div class="col-6 text-center">
                    <div class="alert @if($produto->oferta!='') alert-success @else alert-info @endif text-center" role="alert">
                        @if($produto->oferta!='')<s>@endif R$: {{$produto->valorFormatado()}} @if($produto->oferta!='')</s>  R$: {{$produto->valorFormatado('oferta')}} @endif
                    </div>
                    <a href="adicionar/{{$produto->id}}" class="btn btn-success btn-lg">
                        Adicionar ao Carrinho
                    </a>
                </div>
                <div class="col-12 mt-3">
                    <div class="card">
                        <div class="card-body">
                            <p>{{$produto->descricao}}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-6 text-center">
                    <h3>Categorias</h3>
                    <ul class="list-group">
                    @foreach($produto->categorias as $categoria)
                        <li class="list-group-item">{{$categoria->nome}}</li>
                    @endforeach
                    </ul>
                </div>
                <div class="col-6 text-center">
                    <h3>Características</h3>
                    @foreach($produto->caracteristicas as $caracteristica)
                        <li class="list-group-item">{{$caracteristica->nome}}: {{$caracteristica->pivot->valor}} {{$caracteristica->unidade}}</li>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

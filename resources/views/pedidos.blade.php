@extends('layouts.app', ['current' => 'home'])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
            @foreach($pedidos as $pedido)
                <tr>
                    <td><a href="/pedido/{{$pedido->id}}">{{ \Carbon\Carbon::parse($pedido->updated_at)->format('d/m/Y h:i:s')}}</a></td>
                    <td><a href="/pedido/{{$pedido->id}}">R$ {{$pedido->total}}</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

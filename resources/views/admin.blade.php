@extends('layouts.app', ['current' => 'admin'])

@section('content')

<div class="container">
    @component('layouts.admin.menu', ['current' => ''])
    @endcomponent
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">Administração da Loja</div>

                <div class="card-body">
                    Aqui você pode gerenciar a sua loja, utilize o menu acima navegar.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<form class="form row mb-3" method="GET" role="search">
    <div class="col-10">
        <input type="search" class="form-control mb-2 mr-sm-2 mb-sm-0" name="buscar" placeholder="Digite sua busca" value="{{ \Request::get('buscar') }}">
    </div>
    <div class="col-2 text-right">
        <button type="submit" class="btn btn-primary">Buscar</button>
    </div>
</form>
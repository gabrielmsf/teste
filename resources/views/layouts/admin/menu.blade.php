<div class="row">
    <div class="col">
        <nav class="navbar navbar-expand-lg navbar-light bg-light mb-4">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item @if($current=='categoria') active @endif">
                    <a class="nav-link" href="/admin/categorias">Categorias @if($current=='categoria') <span class="sr-only">(current)</span> @endif</a>
                </li>
                <li class="nav-item @if($current=='caracteristica') active @endif">
                    <a class="nav-link" href="/admin/caracteristicas">Características @if($current=='caracteristica') <span class="sr-only">(current)</span> @endif</a>
                </li>
                <li class="nav-item @if($current=='produto') active @endif">
                    <a class="nav-link" href="/admin/produtos">Produtos @if($current=='produto') <span class="sr-only">(current)</span> @endif</a>
                </li>
                <li class="nav-item @if($current=='pedido') active @endif">
                    <a class="nav-link" href="/pedidos/admin">Pedidos @if($current=='pedido') <span class="sr-only">(current)</span> @endif</a>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div class="container mt-3">
            <div class="row">
                <div class="col-6">
                    <h2 href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </h2>
                </div>
                <div class="col-6 text-right">
                    @guest
                        <a href="{{ route('login') }}"><i class="fa fa-user fa-2x" aria-hidden="true"></i></a>
                    @else
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                           Olá {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/carrinho">
                                Carrinho
                            </a>

                            <a class="dropdown-item" href="/pedidos">
                                Pedidos
                            </a>
                            
                            @if(auth()->user()->isAdmin())
                            <a class="dropdown-item" href="/admin">
                                Configurar Loja
                            </a>
                            @endif
                            
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    @endguest
                </div>
            </div>
        </div>

        <nav class="navbar navbar-expand-md navbar-light bg-light">
            <div class="container">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbaPrincipal" aria-controls="navbaPrincipal" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbaPrincipal">
                    <ul class="navbar-nav mr-auto">
                    <li class="nav-item @if($current=='home') active @endif">
                        <a class="nav-link" href="/">Home @if($current=='home') <span class="sr-only">(current)</span> @endif</a>
                    </li>
                    <li class="nav-item dropdown @if($current=='categorias') active @endif">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Categorias
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            @foreach(App::make("App\Http\Controllers\CategoriaController")->lista() as $categoria)
                            <a class="dropdown-item" href="/categoria/{{$categoria->id}}">{{$categoria->nome}}</a>
                            @endforeach
                        </div>
                    </li>
                    <li class="nav-item @if($current=='promocoes') active @endif">
                        <a class="nav-link" href="/promocoes">Promoções</a>
                    </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0" action="/busca" method="GET">
                        <input class="form-control mr-sm-2" type="search" placeholder="Buscar" name="q" aria-label="Search" value="{{ \Request::get('q') }}">
                        <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Buscar</button>
                    </form>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @hasSection('content')
                @yield('content')
            @endif
        </main>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    @hasSection('javascript')
        @yield('javascript')
    @endif
</body>
</html>

<div class="col-md-4">
    <div class="card">
        <img src="/produtos/imagem/{{$id}}" class="d-block mx-auto img-fluid" alt="{{$nome}}"/>
        <div class="card-header">
            {{$nome}}
        </div>
        <div class="card-body">
            <p class="card-text">{{$descricao}}</p>
            <div class="alert @if($oferta!='0,00') alert-success @else alert-info @endif text-center" role="alert">
                @if($oferta!='0,00')<s>@endif R$: {{$preco}} @if($oferta!='0,00')</s>  R$: {{$oferta}} @endif
            </div>
            <a href="/produtos/{{$id}}" class="btn btn-primary btn-block">Ver mais</a>
        </div>
    </div>
</div>